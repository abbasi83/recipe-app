import React, { useState, useEffect } from "react";

import "./App.css";

import RecipeList from "./components/RecipeList";
import { getRecipes } from "./data/api/api";
import RecipeDetails from "./components/RecipeDetails";

function App() {
  const [recipes, setRecipes] = useState([]);
  const [showList, setShowList] = useState(true);

  const [recipe, setRecipe] = useState(null);
  const [searchString, setSearchString] = useState("");
  const [searching, setSearching] = useState(false);
  useEffect(() => {
    async function fetchData() {
      const response = await getRecipes();
      if (!searching) {
        setRecipes(response);
      } else {
        const tokens = searchString.split(/ ,/).filter(Boolean);

        const filtered = response.filter(recipe => {
          let matched = false;
          for (const ingredient in recipe.ingredients) {
            for (const token in tokens) {
              matched = ingredient.includes(token);
              if (matched) {
                break;
              }
            }
          }

          return matched;
        });
        setRecipes(filtered);
      }
    }
    fetchData();
  }, [searching]);

  const handleSearchStringChange = e => {
    e.preventDefault();
    setSearchString(e.target.value);

    if (e.target.value.length === 0) {
      setSearching(false);
    }
  };
  const handleSearchSubmit = e => {
    e.preventDefault();
    setSearching(searchString.length > 0 ? true : false);
  };

  const handleShowDetail = recipe => {
    setRecipe(recipe);
    setShowList(false);
  };
  const handleShowList = () => {
    setRecipe(null);

    setShowList(true);
  };

  return (
    <React.Fragment>
      {showList ? (
        <RecipeList
          recipes={recipes}
          showDetail={handleShowDetail}
          onSearchStringChange={handleSearchStringChange}
          searchStringValue={searchString}
          onSearchSubmit={handleSearchSubmit}
        />
      ) : (
        <RecipeDetails showList={handleShowList} recipe={recipe} />
      )}
    </React.Fragment>
  );
}

export default App;
