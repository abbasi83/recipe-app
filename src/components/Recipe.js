import React from "react";

const Recipe = props => {
  const { recipe, showDetail } = props;

  const { image_url, title, source_url, publisher } = recipe;
  return (
    <React.Fragment>
      <div className="col-10 mx-auto col-md-6 col-lg-4 my-3">
        <div className="card">
          <img
            src={image_url}
            className="img-card-top"
            style={{ height: "14rem" }}
            alt=""
          />
          <div className="card-body text-capitalize">
            <h6>{title}</h6>
            <h6 className="text-warning text-slanted">
              provided by {publisher}
            </h6>
          </div>
          <div className="card-footer">
            <button
              className="btn btn-primary text-capitalize"
              type="button"
              onClick={() => showDetail(recipe)}
            >
              detail
            </button>
            <a
              href={source_url}
              target="_blank"
              rel="noopener noreferrer"
              className="btn btn-success mx-2 text-capitalize"
            >
              recipe url
            </a>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Recipe;
