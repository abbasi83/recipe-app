import React from "react";

const RecipeDetails = props => {
  const { recipe } = props;
  const {
    image_url,
    publisher,
    publisher_url,
    source_url,
    title,
    ingredients
  } = recipe;

  const { showList } = props;

  return (
    <React.Fragment>
      <div className="container">
        <div className="row">
          <div className="col-10 mx-auto col-md-6 my-3">
            <button
              type="button"
              className="btn btn-warning mb-5 text-capitalize"
              onClick={() => showList()}
            >
              back to recipe list
            </button>
            <img src={image_url} alt="recipe" className="d-block w-100" />
          </div>
          <div className="col-10 mx-auto col-md-6 my-3">
            <h6 className="text-uppercase">{title}</h6>
            <h6 className="text-warning text-capitalize text-slanted">
              provided by {publisher}
            </h6>
            <a
              href={publisher_url}
              target="_blank"
              rel="noopener noreferrer"
              className="btn btn-primary mt-2 text-capitalize"
            >
              publiser webpage
            </a>
            <a
              href={source_url}
              target="_blank"
              rel="noopener noreferrer"
              className="btn btn-success mt-2 ml-3 text-capitalize"
            >
              recipe url
            </a>

            <ul className="list-group mt-4">
              <h2 className="mt-3 mb-4">Ingredients</h2>
              {ingredients.map((item, index) => {
                return (
                  <li className="list-group-item text-slanted" key={index}>
                    {item}
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default RecipeDetails;
