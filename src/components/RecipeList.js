import React from "react";

import RecipeSearch from "./RecipeSearch";
import Recipe from "./Recipe";

const RecipeList = props => {
  const {
    recipes,
    showDetail,
    onSearchStringChange,
    searchStringValue,
    onSearchSubmit
  } = props;

  return (
    <React.Fragment>
      <RecipeSearch
        onSearchStringChange={onSearchStringChange}
        searchStringValue={searchStringValue}
        onSearchSubmit={onSearchSubmit}
      />
      <div className="container my-5">
        <div className="row">
          <div className="col-10 mx-auto col-md-6 text-center text-uppercase mb-3">
            <h1 className="text-slanted">recipe list</h1>
          </div>
        </div>
        <div className="row">
          {recipes.map(recipe => {
            return (
              <Recipe
                key={recipe.recipe_id}
                recipe={recipe}
                showDetail={showDetail}
              />
            );
          })}
        </div>
      </div>
    </React.Fragment>
  );
};

export default RecipeList;
