import React from "react";

const RecipeSearch = props => {
  const { onSearchStringChange, searchStringValue, onSearchSubmit } = props;
  return (
    <React.Fragment>
      <div className="container">
        <div className="row">
          <div className="col-10 mx-auto col-md-8 mt-5 text-center">
            <h1 className="text-capitalize text-slanted">
              search for recipe with{" "}
              <strong className="text-danger">Food2Fork</strong>
            </h1>
            <label htmlFor="search" className="text-capitalize">
              type recipes separated by comma
            </label>
            <div className="input-group">
              <input
                type="text"
                className="form-control"
                placeholder="chicken, onions, carrots"
                aria-label=""
                aria-describedby="basic-addon2"
                name="search"
                onChange={onSearchStringChange}
                value={searchStringValue}
              />
              <div className="input-group-append">
                <button
                  type="submit"
                  className="input-group-text bg-primary text-white"
                  onClick={onSearchSubmit}
                >
                  <i className="fas fa-search" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
};

export default RecipeSearch;
